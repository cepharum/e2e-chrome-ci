FROM registry.gitlab.com/cepharum-foss/ci-base

LABEL maintainer="cepharum GmbH"

RUN apk add --no-cache chromium-chromedriver chromium && \
    mkdir -p /home/chrome && adduser -D chrome && chown chrome:chrome /home/chrome

USER chrome
WORKDIR /home/chrome

ENV CHROME_BIN=/usr/bin/chromium-browser \
    CHROME_PATH=/usr/lib/chromium/
