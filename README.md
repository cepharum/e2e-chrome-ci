# Chrome for CI

a dockerized chromedriver for headless E2E testing in CI

## License

MIT

## It's Chromium!

The project was started to provide chrome for CI testing. As a matter of fact chromium is easier to install in Alpine Linux and comes supported in its latest stable release. That's why this project is referring to chrome in its name but actually providing chromium instead.

## Install

This project is providing container image at Docker Hub. You can use this image in CI configuration (GitLab CI) like this:

    image: cepharum/e2e-chrome-ci
    
    test:
      script:
        - npm install
        - npm run test
